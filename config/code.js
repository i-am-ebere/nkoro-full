module.exports = {
  notFound: {
    message: "Are you lost?",
    code: "LS404",
  },
  nkoroNotFound: {
    message: "record unavaliable",
    code: "R404",
  },
  coordsNotFound: {
    message: "coords must be specified",
    code: "R401",
  },
  badCredentials: {
    message: "Invalid Credentials",
    code: "R402",
  },
};
