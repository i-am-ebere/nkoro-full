require("dotenv").config();
const mongoose = require("mongoose");
//DB connection
mongoose.connect(
  process.env.DBR,
  { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true },
  () => console.log("mongodb connected!"),
  (err) => console.log(err)
);

module.exports = mongoose;
