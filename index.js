require("dotenv").config();
const express = require("express");
const app = express();
const PORT = process.env.PORT;
const cors = require("cors");
const errors = require("./config/code");
//---
//mongoose connection
require("./config/db_connect");
//all json requests
app.use(cors());
app.use(express.json());

/* All routes */
app.use("/api/auth", require("./routes/auth.routes"));
app.use("/api/restaurants", require("./routes/restaurants.routes"));

//All other routes
app.get("*", (req, res) => {
  res.json(errors.notFound).status(404);
});

app.listen(PORT, () => console.log(`running on ${PORT}`));
