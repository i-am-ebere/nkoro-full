const router = require("express").Router();
const errorCode = require("../config/code");
const { kmToRadian, distance } = require("../config/config");
const Restaurant = require("../models/restaurant.model");
const Cuisine = require("../models/cuisine.model");

/* 
    @route GET /api/restaurants/featured
    @desc  View all within 15km
    @access Private 
*/
router.get("/featured", async (req, res) => {
  const lat = parseFloat(req.query.lat);
  const long = parseFloat(req.query.long);
  const maxDistance = parseFloat(req.query.maxDistance) || 15;
  try {
    let restaurants = await Restaurant.aggregate([
      {
        $match: {
          // isActive: true,
          isFeatured: true,
        },
      },
      {
        $geoNear: {
          near: { type: "Point", coordinates: [lat, long] },
          distanceField: "distance",
          maxDistance: maxDistance * 1000,
          spherical: true,
          distanceMultiplier: 1 / 1000,
        },
      },
      {
        $lookup: {
          from: "cuisines",
          localField: "cuisines",
          foreignField: "_id",
          as: "cuisines",
        },
      },
    ]);

    if (restaurants.length < 1) {
      restaurants = await Restaurant.find({ city: req.body.city }).populate(
        "cuisines"
      );
    }
    res
      .json({
        restaurants,
        meta: { count: restaurants.length, status: "success" },
      })
      .status(200);
  } catch (error) {
    console.log(error);
    res.json({ err: "" }).status(200);
  }
});

/* 
    @route GET /api/restaurants
    @desc  View all within 15km
    @access Private 
*/
router.get("/", async (req, res) => {
  const lat = parseFloat(req.query.lat);
  const long = parseFloat(req.query.long);
  const city = req.query.city ? req.query.city.toLowerCase() : "abuja";
  const maxDistance = parseFloat(req.query.maxDistance) || 20000;
  console.log("lat:", lat);
  console.log("long:", long);
  try {
    if (!lat && !long) {
      return res
        .status(400)
        .json({ message: "Habibi Please specify Lat or long" });
    }

    let restaurants = await Restaurant.aggregate([
      {
        $geoNear: {
          near: {
            // $geometry: {
            type: "Point",
            coordinates: [long, lat],
            // },
            // $maxDistance: maxDistance,
          },
          distanceField: "distance",
          maxDistance: maxDistance,
          spherical: true,
          distanceMultiplier: 1 / 1000,
        },
      },
      {
        $sort: {
          distance: 1,
        },
      },
      {
        $lookup: {
          from: "cuisines",
          localField: "cuisines",
          foreignField: "_id",
          as: "cuisines",
        },
      },
    ]);

    if (restaurants.length < 1) {
      restaurants = await Restaurant.find({
        $and: [{ isActive: true }, { city: city }],
      }).populate({
        path: "cuisines",
        select: "name",
        model: "Cuisine",
      });
      console.log("city", city);
      /* if restaurants are still empty return empty */
      if (restaurants.length < 1) {
        return res.status(200).json({
          meta: {
            count: restaurants.length,
            type: "all",
            status: "success",
            message: "nothing found!",
          },
          restaurants,
        });
      }

      return res.status(200).json({
        meta: {
          count: restaurants.length,
          type: "city",
          status: "success",
          message: "nothing around you found!",
        },
        restaurants,
      });
    }

    res
      .json({
        restaurants,
        meta: {
          count: restaurants.length,
          status: "success",
          message: "successfully found!",
          type: "query",
        },
      })
      .status(200);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "server error, reload page", e: error });
  }
});

/* 
    @route GET /api/restaurants/search
    @desc  View all
    @access Private 
    @query keyword / 
    Divine or unity or 
*/
router.post("/search", async (req, res) => {
  const lat = parseFloat(req.query.lat);
  const long = parseFloat(req.query.long);
  const city = req.query.city ? req.query.city.toLowerCase() : "abuja";
  const cuisine = req.query.cuisine ? req.query.cuisine.split(",") : "abuja";
  const keyword = req.query.keyword ? req.query.keyword.toLowerCase() : "abuja";
  const maxDistance = parseFloat(req.query.maxDistance) || 20000;

  if (req.body.coords) {
    let coords = { type: "Point", coordinates: req.body.coords };

    try {
      let restaurant = await Restaurant.find();
      restaurant.location = coords;
      restaurant.isActive = true;
      //   console.log(restaurant);

      await restaurant.save();
      res.json(restaurant);
    } catch (error) {}
  } else {
    return res.json(errorCode.coordsNotFound).status(200);
  }
});

/* 
    @route POST /api/restaurants
    @desc  View all
    @access Private 
*/
router.post("/create", async (req, res) => {
  if (req.body.coords) {
    let coords = { type: "Point", coordinates: req.body.coords };

    try {
      let restaurant = new Restaurant(req.body);
      restaurant.location = coords;
      restaurant.isActive = true;
      //   console.log(restaurant);

      await restaurant.save();
      res.json(restaurant);
    } catch (error) {}
  } else {
    return res.json(errorCode.coordsNotFound).status(200);
  }
});

/* 
    @route GET /api/restaurants/filter
    @desc  filter by name
    @access Private 
*/
router.get("/:id", async (req, res) => {
  console.log(req.params.id);
  try {
    let restaurant = await Restaurant.findById(req.params.id, {
      isActive: true,
    })
      .populate({
        path: "cuisines",
        select: "name",
        model: "Cuisine",
      })
      .populate({
        path: "comments",
        populate: {
          path: "user",
          model: "User",
        },
      });
    if (!restaurant) {
      res.json(errorCode.nkoroNotFound).status(404);
    }

    res.json({ restaurant, meta: { status: "success" } }).status(200);
  } catch (error) {
    if (error.name == "CastError") {
      return res.json({ message: "params wrong" }).status(401);
    }
    res.json({}).status(401);
  }
});

module.exports = router;
