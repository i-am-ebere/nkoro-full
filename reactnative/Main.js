import React from "react";
import { View, Text } from "react-native";
import HomeScreen from "./screens/HomeScreen";
import SingleNkoro from "./screens/SingleNkoro";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import BottomNavigation from "./navigation/BottomNavigation";

let Stack = createStackNavigator();

const Main = () => {
  return (
    <>
      <NavigationContainer>
        <Stack.Navigator headerMode="none">
          <Stack.Screen name="Root" component={BottomNavigation} />
          <Stack.Screen name="Nkoro" component={SingleNkoro} />
        </Stack.Navigator>
      </NavigationContainer>

      {/* <SingleNkoro /> */}
    </>
  );
};

export default Main;
