import Axios from "axios";
import { AsyncStorage } from "react-native";

export const getNkoroByLocation = async () => {
  //   if (!lat && !long) {
  const lat = await AsyncStorage.getItem("lat");
  const long = await AsyncStorage.getItem("long");
  const city = await AsyncStorage.getItem("city");
  //   }
  return await Axios.get(
    `https://nkoro2.herokuapp.com/api/restaurants?lat=${lat}&long=${long}&city=${city}`
  );
};

export const getFeaturedNkoro = async (coords) => {
  return await Axios.get(
    "https://nkoro2.herokuapp.com/api/restaurants?lat=21.516&long=39.1794"
  );
};

export const getSeachNkoroByName = async ({ keyword, cuisine }) => {
  const city = await AsyncStorage.getItem("city");
  const lat = await AsyncStorage.getItem("lat");
  const long = await AsyncStorage.getItem("long");

  return await Axios.get(
    `https://nkoro2.herokuapp.com/api/restaurants/search?keyword=${keyword}&lat=${lat}&long=${long}&city=${city}&cuisine=${cuisine}`
  );
};
