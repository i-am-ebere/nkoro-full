import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, AsyncStorage } from "react-native";
import Main from "./Main";

import * as Location from "expo-location";
import * as Permissions from "expo-permissions";

export default function App() {
  const [errorMsg, setErrorMsg] = useState("");
  const [city, setCity] = useState("");
  const [statusOfCall, setStatus] = useState(false);

  const _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      setErrorMsg("Permission to access location was denied");
      return false;
    }
    try {
      let location = await Location.getCurrentPositionAsync({});
      const { latitude, longitude } = location.coords;
      await getGeocodeAsync({ latitude, longitude });
      await AsyncStorage.setItem("city", city);

      await AsyncStorage.setItem("lat", `${latitude}`);
      await AsyncStorage.setItem("long", `${longitude}`);

      let x = await AsyncStorage.getItem("city");
      if (x) {
        setStatus(true);
        return false;
      }
      return true;
    } catch (error) {
      setErrorMsg("Permission to access location was denied");
    }
  };

  const getGeocodeAsync = async (location) => {
    let geocode = await Location.reverseGeocodeAsync(location);
    setCity(geocode[0].city);
  };

  useEffect(() => {
    _getLocationAsync();
  }, [_getLocationAsync, city, setCity]);

  if (!statusOfCall) {
    return <Text>No Location Selected</Text>;
  }

  return <Main msg={errorMsg} status={statusOfCall} />;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
