import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { ListItem } from "react-native-elements";

const list = [
  {
    title: "Appointments",
    icon: "av-timer",
  },
  {
    title: "About",
    icon: "flight-takeoff",
  },
];

const SettingScreen = () => {
  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          flex: 1,
          paddingTop: 100,
          justifyContent: "center",
          paddingHorizontal: 15,
        }}
      >
        <Text style={{ fontSize: 22, fontWeight: "bold" }}> Settings</Text>
      </View>
      <View style={{ flex: 5 }}>
        {list.map((item, i) => (
          <ListItem
            key={i}
            title={item.title}
            leftIcon={{ name: item.icon }}
            bottomDivider
            chevron
          />
        ))}
      </View>
    </View>
  );
};

export default SettingScreen;

const styles = StyleSheet.create({});
