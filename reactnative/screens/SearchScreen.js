import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  AsyncStorage,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { TextInput, ScrollView, FlatList } from "react-native-gesture-handler";
import { Image, Badge } from "react-native-elements";
// import imgs from "../assets/img/bounce1.jpg";
// import imgss from "../assets/img/bounce2.jpg";
// import imgsss from "../assets/img/bounce3.jpg";
// import imgssss from "../assets/img/bounce4.jpg";

import Header from "../components/Header";
import NearByNkoro from "../components/NearByNkoro";
import { getNkoroByLocation, getSeachNkoroByName } from "../utliz/api";

const SearchScreen = () => {
  const [nkoros, setNkoros] = useState([]);
  const [search, setSearch] = useState({
    keyword: "",
  });
  const [isLoading, setLoading] = useState(true);

  async function searchNkoros() {
    getSeachNkoroByName({ search })
      .then((res) => {})
      .catch((error) => {});
  }

  async function nearByNkoros() {
    let latLong = await getLatLong();
    setLoading(true);
    getNkoroByLocation(latLong)
      .then((data) => {
        console.log("complete: ", data);
        setNkoros(data.data.restaurants);

        setLoading(false);
      })
      .catch((err) => {
        console.log(err.response);
      });
  }

  async function getLatLong() {
    const lat = await AsyncStorage.getItem("lat");
    const long = await AsyncStorage.getItem("long");

    return { lat, long };
  }

  useEffect(() => {
    let mounted = true;
    setLoading(true);

    getNkoroByLocation()
      .then((data) => {
        console.log("search page", data);
        if (mounted) {
          setNkoros(data.data.restaurants);
        }

        setLoading(false);
      })
      .catch((err) => {
        console.log(err.response);

        setLoading(false);
      });

    return () => {
      mounted = false;
    };
  }, [setNkoros]);

  function changeHandler(e, name, value) {
    // console.log(e);
    e.persist();
    // console.log("name : ", name);
    // console.log("value :", e.nativeEvent.text);
    setSearch({ ...search, [name]: e.nativeEvent.text });
  }

  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: "center" }}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={{ flex: 4 }}>
          <Header home={true} />
        </View>
        <View style={styles.searchContainer}>
          <Ionicons name="ios-search" size={22} />
          <TextInput
            placeholder={"Search..."}
            style={{ marginHorizontal: 10, flex: 1 }}
            onChange={(e) => changeHandler(e, "keyword")}
            name="keyword"
            value={search.keyword}
          />
        </View>
        {/* <Text>Text: {search.keyword}</Text> */}
      </View>
      <View style={styles.content}>
        <View style={{ flex: 1 }}>
          {nkoros.length < 1 ? (
            <View style={{ flex: 1, justifyContent: "center" }}>
              <Text> No Nkoros Around you!</Text>
            </View>
          ) : (
            <FlatList
              data={nkoros}
              showsVerticalScrollIndicator={false}
              renderItem={({ item }) => <NearByNkoro info={item} />}
              keyExtractor={(item) => item._id}
              refreshing={<ActivityIndicator size="large" color="#0000ff" />}
              onRefresh={nearByNkoros}
            />
          )}
        </View>
      </View>
    </View>
  );
};

export default SearchScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#E2445B",
    flex: 1,
  },
  header: {
    paddingTop: 30,
    // flex: 2,
    height: 200,
    marginHorizontal: 10,
  },
  searchContainer: {
    backgroundColor: "#fff",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    // borderRadius: 10,
    marginTop: 15,
    paddingVertical: 9,
    paddingHorizontal: 10,
    marginBottom: 15,
  },
  content: {
    backgroundColor: "#F7F6F6",
    // marginHorizontal: 10,
    paddingHorizontal: 15,
    paddingTop: 15,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    flex: 4,
    // paddingBottom: 250
  },
  popular: {
    width: 150,
    height: 150,
    borderRadius: 10,
    backgroundColor: "red",
    marginRight: 10,
    overflow: "hidden",
  },
});
