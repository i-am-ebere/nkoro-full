import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import Header from "../components/Header";
import { Image, Button, Icon } from "react-native-elements";
import Comment from "../components/Comment";
import { FloatingAction } from "react-native-floating-action";

import imgss from "../assets/img/bounce2.jpg";

const SingleNkoro = ({ route }) => {
  const { details } = route.params;
  // console.log(details);

  return (
    <View style={{ flex: 1 }}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          // flexGrow: 1,
          paddingBottom: 320,
          // paddingVertical: 20
        }}
      >
        <View style={[styles.header, { position: "relative" }]}>
          <Image
            source={{ uri: details.image }}
            // tintColor="#2d3436"
            style={{
              width: null,
              resizeMode: "cover",
              height: "100%",
              //   tintColor: "#2d3436"
            }}
          ></Image>
          <View
            style={{
              position: "absolute",
              width: "100%",
            }}
          >
            <View style={{ marginHorizontal: 15, marginTop: 25 }}>
              <Header color="#000" />
            </View>
          </View>
        </View>
        <View style={styles.content}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              marginVertical: 15,
            }}
          >
            <View>
              <Text style={{ fontSize: 22, fontWeight: "bold" }}>
                {details.name}
              </Text>
              <Text>Igbo, Yoruba, Hausa, Calabar</Text>
            </View>
            <View>
              <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                {/* {info.distance} */}
                {details.distance ? `${details.distance.toFixed()}km` : `0km`}
              </Text>
            </View>
          </View>
          <View style={{ marginBottom: 15 }}>
            <View
              style={{
                marginHorizontal: 30,
                flexDirection: "row",
                justifyContent: "space-evenly",
              }}
            >
              <View style={styles.ratings}>
                <Text style={styles.ratingNumber}>{details.rating.clean}</Text>
                <Text>Clean</Text>
              </View>
              <View style={styles.ratings}>
                <Text style={styles.ratingNumber}>{details.rating.taste}</Text>
                <Text>Taste</Text>
              </View>
              <View style={styles.ratings}>
                <Text style={styles.ratingNumber}>
                  {details.rating.service}
                </Text>
                <Text>Service</Text>
              </View>
              <View style={styles.ratings}>
                <Text style={styles.ratingNumber}>{details.rating.price}</Text>
                <Text>Price</Text>
              </View>
            </View>
          </View>
          <View style={{ marginVertical: 15 }}>
            <Button
              title="Get Directions"
              onPress={() => {}}
              buttonStyle={{ backgroundColor: "#ff5c62", paddingVertical: 20 }}
            />
          </View>
          <View>
            {details.comments.length > 0 ? (
              details.comments.map((comment, index) => (
                <Comment key={index} comment={comment} />
              ))
            ) : (
              <View>
                <Text>Nobody don yarn anything yet</Text>
              </View>
            )}
          </View>
        </View>
      </ScrollView>
      <FloatingAction
        color="#ff5c62"
        onPressItem={(name) => {
          console.log(`selected button: ${name}`);
          return alert("Ekite");
        }}
      />
      {/* <TouchableOpacity onPress={() => alert("FAB clicked")} style={styles.fab}>
        <Icon name="map-marker" size={25} color="#8E8E93" />
      </TouchableOpacity> */}
    </View>
  );
};

export default SingleNkoro;

const styles = StyleSheet.create({
  header: {
    // flex: 1,
    height: 300,
  },
  content: {
    flex: 2,
    paddingHorizontal: 15,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  ratings: {
    paddingHorizontal: 5,
    textAlign: "center",
  },
  ratingNumber: {
    fontSize: 22,
    fontWeight: "bold",
    textAlign: "center",
  },
  fab: {
    width: 56,
    height: 56,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    // right: 20,
    // top: 20,
    backgroundColor: "#03A9F4",
    borderRadius: 30,
    elevation: 8,
  },
});
