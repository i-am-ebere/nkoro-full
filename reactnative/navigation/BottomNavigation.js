import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import HomeScreen from "../screens/HomeScreen";
import SearchScreen from "../screens/SearchScreen";
import { Icon } from "react-native-elements";
import SettingScreen from "../screens/SettingScreen";

const BottomTab = createBottomTabNavigator();
const BottomNavigation = () => {
  return (
    <BottomTab.Navigator>
      <BottomTab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarIcon: (focus) => (
            <Icon
              name="home"
              size={25}
              type="font-awesome"
              // color={color}
            />
          ),
        }}
      />
      <BottomTab.Screen
        name="Search Nkoros"
        component={SearchScreen}
        options={{
          tabBarIcon: (focus) => (
            <Icon
              name="search"
              size={25}
              type="font-awesome"
              // color={color}
            />
          ),
        }}
      />
    </BottomTab.Navigator>
  );
};

export default BottomNavigation;
