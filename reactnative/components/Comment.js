import React from "react";
import { StyleSheet, Text, View } from "react-native";

const Comment = ({ comment }) => {
  // console.log("cp", comment);
  return (
    <View
      style={{
        marginBottom: 15,
        borderBottomColor: "#000",
        borderBottomWidth: 0.2,
        paddingBottom: 20,
        borderStyle: "solid",
      }}
    >
      <View
        style={{
          flexDirection: "row",
        }}
      >
        <View
          style={{
            borderRadius: 10,
            width: 50,
            height: 50,
            backgroundColor: "#000",
            marginRight: 15,
          }}
        ></View>
        <View>
          <Text>{comment.name}</Text>
          <Text>20 mins ago.</Text>
        </View>
      </View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          marginVertical: 9,
        }}
      >
        <Text>Clean: {comment.rating.clean}</Text>
        <Text>Price: {comment.rating.price}</Text>
        <Text>Service: {comment.rating.service}</Text>
        <Text>Taste: {comment.rating.taste}</Text>
      </View>
      <View>
        <Text>{comment.comment}</Text>
      </View>
    </View>
  );
};

export default Comment;

const styles = StyleSheet.create({});
