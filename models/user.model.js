const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    first_name: String,
    last_name: String,
    email: {
      type: String,
      unique: true,
    },
    phone: {
      type: String,
      unique: true,
    },
    password: String,
    avatar: String,
    username: {
      type: String,
      unique: true,
    },
    user_type: {
      type: String,
      enum: ["admin", "owner", "normal"],
      default: "normal",
    },
    facebook_token: String,
    twitter_token: String,
    instagram_token: String,
  },
  { timestamps: true }
);

const User = mongoose.model("User", userSchema);
module.exports = User;
